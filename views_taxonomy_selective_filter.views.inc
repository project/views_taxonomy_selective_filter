<?php
/**
 * @file views_taxonomy_selective_filter.views.inc
 * Views hooks for the module.
 */

/**
 * Implementation of hook_views_data_alter().
 *
 * Hijack the term tid filter with our own handler. Mwahaha.
 */
function views_taxonomy_selective_filter_views_data_alter(&$data) {
 $data['term_node']['tid']['filter']['handler'] = 'views_taxonomy_selective_filter_filter_term_node_tid_selective';
}


/**
 * Implementation of hook_views_handlers().
 */
function views_taxonomy_selective_filter_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'views_taxonomy_selective_filter'),
    ),
    'handlers' => array(
      'views_taxonomy_selective_filter_filter_term_node_tid_selective' => array(
        'parent' => 'views_handler_filter_term_node_tid',
      ),
    ),
  );
}
