<?php

/**
 * Filter by term id.
 *
 * Replaces the exposed term filter with one that shows only taxonomy terms 
 * that are in the original result set (that is, the result of the view without 
 * this filter in consideration).
 * 
 * This is best explained by an example:
 *
 * Consider a site with these nodes:
 *  node alpha  terms: A, B, 
 *  node beta   terms: B, C
 *  node gamma  terms: A, D
 *  node omega  terms: Z
 *
 * Suppose our view returns the first three nodes but not node omega.
 * The ordinary exposed term filter will show: (A B C D Z).
 * This filter will show: (A B C D), because Z is not in the result set.
 * Now suppose we set the exposed filter to 'B'. The filter will still show the
 * same list (A B C D), as the original result set is considered.
 *
 * It is ESSENTIAL that this filter be LAST in the filters on the view, so other
 * filters are already on the query and this filter is not.
 * @todo: perform validation to make sure this is the case.
 */
class views_taxonomy_selective_filter_filter_term_node_tid_selective extends views_handler_filter_term_node_tid {

  /*
  // debug: check this handler is being used.
  function Xinit(&$view, $options) {
    parent::init($view, $options);
    dsm(__FUNCTION__);
    dsm($this);
  }
  */

  /**
   * Add our default options for exposed filters.
   */
  function expose_options() {
    parent::expose_options();
    
    $this->options['expose']['selective'] = FALSE;
  }

  /**
   * Add our checkbox to the 'right' side of the exposed options form.
   */
  function expose_form_right(&$form, &$form_state) {
    parent::expose_form_right($form, $form_state);
    
    $form['expose']['selective'] = array(
      '#type' => 'checkbox',
      '#title' => t('Limit list to terms in the view result'),
      '#description' => t("If checked, the terms shown in the filter will be only those that are applied to nodes in the view's original result."),
      '#default_value' => $this->options['expose']['selective'],
    );
  }

  /**
   * Validates the handler against the complete View.
   *
   * Verify that we are the last filter in every display if we are using our
   * selective option.
   *
   * @return
   *   Empty array if the handler is valid; an array of error strings if it is not.
   */
  function validate() {
    $errors = parent::validate();
    // The key we are looking for in the $display->display_options['filters'] array.
    // WARNING: this may fail if the view is on a non-node base with a relationship to the node table.
    // However, it'll be a false positive validation rather than false negative, ie will validate
    // things that should not. 
    $filter_key = $this->field;
    
    // We don't know which display we are on and so we must check them all.
    foreach ($this->view->display as $display) {
      // Only consider displays that have filters, have our filter on them, and have it exposed and selective.
      if (isset($display->display_options['filters']) && 
        isset($display->display_options['filters'][$filter_key]) &&
        $display->display_options['filters'][$filter_key]['exposed'] && 
        $display->display_options['filters'][$filter_key]['expose']['selective']
      ) {
        // Get the last of the filters on this display.
        $last_filter = end($display->display_options['filters']);

        if (!($last_filter['id'] == $filter_key && $last_filter['table'] == $this->table)) {
          $errors[] = t('The exposed taxonomy filter must be set to be the last filter in the %display display (id %id) to function correctly.', array(
            '%display'  => $display->display_title,
            '%id'       => $display->id,
          ));
        }
      }
    }

    return $errors;
  }

  /**
   * Return the terms from the original view result.
   * 
   * We want to run the same query as the main view, minus ourselves. 
   * The idea is that we want the complete result set (not just this page),
   * but not be limited by the current selected value in this exposed filter.
   * We need to be able to pick out terms from this result, and use these to 
   * populate (or trim, rather) the exposed filter dropdown.
   */
  function get_original_terms() {
    // Clone the query object so we can muck about with it with impunity.
    $clone_query = drupal_clone($this->view->query);
    
    //dsm($clone_query);
    
    // Add the term_node.tid field with a totally safe alias so we get terms
    // in our results. We actively want multiple rows per node here so we
    // can easily pick out all the terms.
    $clone_query->add_field('term_node', 'tid', 'term_node_tid_our_alias');
    
    // Add a WHERE clause to limit the terms returned to the vocabulary we 
    // are limited to: in other words, only return terms that were in the form
    // in the first place.
    $term_data_alias = $clone_query->add_table('term_data');
    $clone_query->add_where(0, "$term_data_alias.vid = %d", $this->options['vid']);

    // Build the query to get the SQL string.
    $query_string = $clone_query->query();
    
    // Get the arguments for the query (ie the values for the placeholders).
    $query_args = $clone_query->get_where_args();
    
    // Run the query and its arguments through hook_views_query_substitutions.
    // This ensures that any filters that depend on the date ('***CURRENT_TIME***')
    // get properly substituted.
    $replacements = module_invoke_all('views_query_substitutions', $this);
    $query_string = str_replace(array_keys($replacements), $replacements, $query_string);
    if (is_array($query_args)) {
      foreach ($query_args as $id => $arg) {
        $query_args[$id] = str_replace(array_keys($replacements), $replacements, $arg);
      }
    }
    
    //dsm($query_string);
    //dsm($query_args);
    
    // Run the query ourselves.
    $result = db_query($query_string, $query_args);
    while ($item = db_fetch_array($result)) {
      // Build an array of terms, using the key to ensure uniqueness.
      $result_terms[$item['term_node_tid_our_alias']] = $item['term_node_tid_our_alias'];
    }
    //dsm($result_terms);
    
    return $result_terms;
  }

  /**
   * Some sort of method that considers the input from the exposed filter.
   * (docs missing from Views... :/ )
   *
   * We do the work here because if the filter form element is set to 'Any' then 
   * we never get to query(), or in fact any of the methods that have enough
   * of a view already built to work on.
   */
  function accept_exposed_input($input) {   
    // Only act if the option is set.
    if ($this->options['expose']['selective']) {
      // Go and mess around with the cached form.
      // Get the form out from Views' own cache. 
      // $view->build() has already built the form and stashed it here.
      $form = views_exposed_form_cache($this->view->name, $this->view->current_display);
      
      // Get the terms for the original view result.
      $result_terms = $this->get_original_terms();
      
      // Build a new list of options.
      $new_options = array();
      
      if (count($result_terms)) {
        // A single-valued filter needs an 'Any' option at the top.
        if ($this->options['expose']['single']) {
          $new_options['All'] = $form['tid']['#options']['All'];
        }
  
        foreach ($result_terms as $tid) {
          $new_options[$tid] = $form['tid']['#options'][$tid];
        }
      }
      else {
        // There were no results from the query at all.
        foreach ($form['tid']['#options'] as $key => $value) {
          if (is_numeric($key)) {
            $first_key = $key;
            break;
          }
        }

        // This is a horrendous kludge that takes the first numeric tid (ie the first tid)
        // from the options and limits the options to just that, with a spoofed
        // label of 'No results'. 
        // This is because we have to have the dropdown show SOMETHING, and keys
        // such as 0 or NULL will produce an error if the user tries to submit
        // the form.
        $new_options[$first_key] = t('<No results>');
      }

      // Put the new options into the form
      $form['tid']['#options'] = $new_options;

      // Put the form back in the cache now we have doctored it.
      views_exposed_form_cache($this->view->name, $this->view->current_display, $form);

      // This causes the form to be built again, and this time it will be retrieved 
      // from the view cache where we just put it.
      $this->view->exposed_widgets = $this->view->render_exposed_form();
    }
    
    // Call the parent method and let it do its normal stuff.
    return parent::accept_exposed_input($input);
  }
}
